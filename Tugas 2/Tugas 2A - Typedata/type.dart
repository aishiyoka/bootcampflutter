// //Soal No.1
// void main() {
//   var word = 'dart';
//   var second = 'is';
//   var third = 'awesome';
//   var fourth = 'and';
//   var fifth = 'I';
//   var sixth = 'love';
//   var seventh = 'it!';

//   // Menggabungkan kata-kata menggunakan interpolation
//   var sentence =
//       '${_capitalize(word)} $second ${third} $fourth $fifth ${sixth} $seventh';

//   print(sentence); // Output: Dart is awesome and I love it!
// }

// // Membuat fungsi untuk mengkapitalisasi kata pertama
// String _capitalize(String word) {
//   return word[0].toUpperCase() +
//       word.substring(1); // Mengubah huruf pertama menjadi kapital
// }

// //Soal No.2
// void main() {
//   var sentence = "I am going to be Flutter Developer";

//   // Memisahkan kalimat menjadi kata-kata
//   var words = sentence.split(' ');

//   // Mengambil kata-kata yang diperlukan berdasarkan indeks
//   var exampleFirstWord = words[0];
//   var exampleSecondWord = words[1];
//   var thirdWord = words[2];
//   var fourthWord = words[3];
//   var fifthWord = words[4];
//   var sixthWord = words[5];
//   var seventhWord = words[6];

//   // Mencetak hasil sesuai dengan format yang diinginkan
//   print('First Word: ' + exampleFirstWord);
//   print('Second Word: ' + exampleSecondWord);
//   print('Third Word: ' + thirdWord);
//   print('Fourth Word: ' + fourthWord);
//   print('Fifth Word: ' + fifthWord);
//   print('Sixth Word: ' + sixthWord);
//   print('Seventh Word: ' + seventhWord);
// }

// //Soal No.3
// import 'dart:io';

// void main() {
//   // Minta pengguna untuk memasukkan nama depan
//   stdout.write('Masukkan nama depan: ');
//   String firstName = stdin.readLineSync()!;

//   // Minta pengguna untuk memasukkan nama belakang
//   stdout.write('Masukkan nama belakang: ');
//   String lastName = stdin.readLineSync()!;

//   // Gabungkan nama depan dan nama belakang menjadi nama lengkap
//   String fullName = '$firstName $lastName';

//   // Cetak nama lengkap
//   print('Nama lengkap anda adalah: $fullName');
// }

//Soal No. 4
void main() {
  int a = 5;
  int b = 10;

  // Operasi perkalian
  int multiplicationResult = a * b;
  print('$a * $b = $multiplicationResult');

  // Operasi penjumlahan
  int additionResult = a + b;
  print('$a + $b = $additionResult');

  // Operasi pengurangan
  int subtractionResult = a - b;
  print('$a - $b = $subtractionResult');

  // Operasi pembagian (hasil float)
  double divisionResult = a / b;
  print('$a / $b = $divisionResult');
}
