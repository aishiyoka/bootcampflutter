void main() {
  int a = 5;
  int b = 10;

  // Operasi perkalian
  int multiplicationResult = a * b;
  print('$a * $b = $multiplicationResult');

  // Operasi penjumlahan
  int additionResult = a + b;
  print('$a + $b = $additionResult');

  // Operasi pengurangan
  int subtractionResult = a - b;
  print('$a - $b = $subtractionResult');

  // Operasi pembagian (hasil float)
  double divisionResult = a / b;
  print('$a / $b = $divisionResult');
}
