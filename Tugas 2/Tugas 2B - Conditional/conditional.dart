// //Soal No. 1
// import 'dart:io';

// void main() {
//   stdout.write('Apakah Anda ingin menginstall aplikasi? (Y/T): ');
//   String? answer = stdin.readLineSync();

//   // Menggunakan kondisi ternary untuk menentukan pesan berdasarkan input
//   String message = (answer?.toUpperCase() == 'Y')
//       ? 'Anda akan menginstall aplikasi Dart'
//       : 'Aborted';

//   print(message);
// }

// //Soal No. 2
// import 'dart:io';

// void main() {
//   stdout.write('Masukkan nama kamu: ');
//   String? nama = stdin.readLineSync();

//   // Periksa apakah nama diisi
//   if (nama == null || nama.isEmpty) {
//     print('Warning: Nama harus diisi!');
//     return; // Menghentikan program jika nama tidak diisi
//   }

//   stdout.write('Masukkan peran kamu (penyihir/guard/werewolf): ');
//   String? peran = stdin.readLineSync();

//   // Periksa apakah peran diisi
//   if (peran == null || peran.isEmpty) {
//     print('Warning: Pilih Peranmu untuk memulai game!');
//     return; // Menghentikan program jika peran tidak diisi
//   }

//   // Respons game berdasarkan peran yang dipilih
//   switch (peran.toLowerCase()) {
//     case 'penyihir':
//       print('Halo $nama, selamat datang di dunia Werewolf!');
//       print('Sebagai penyihir, gunakan kekuatanmu untuk melindungi warga desa.');
//       break;
//     case 'guard':
//       print('Halo $nama, selamat datang di dunia Werewolf!');
//       print('Sebagai guard, tugasmu adalah melindungi warga desa dari serangan werewolf.');
//       break;
//     case 'werewolf':
//       print('Halo $nama, selamat datang di dunia Werewolf!');
//       print('Sebagai werewolf, kamu harus berusaha menyembunyikan identitasmu dan menyerang warga desa secara rahasia.');
//       break;
//     default:
//       print('Peran yang kamu pilih tidak valid. Pilih antara penyihir, guard, atau werewolf.');
//       break;
//   }
// }

// //Soal No. 3
// import 'dart:io';

// void main() {
//   stdout.write('Masukkan hari (Senin/Minggu): ');
//   String? hari = stdin.readLineSync();

//   // Menggunakan switch case untuk menampilkan quote berdasarkan hari
//   switch (hari?.toLowerCase()) {
//     case 'senin':
//       print(
//           'Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.');
//       break;
//     case 'selasa':
//       print(
//           'Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.');
//       break;
//     case 'rabu':
//       print(
//           'Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.');
//       break;
//     case 'kamis':
//       print(
//           'Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.');
//       break;
//     case 'jumat':
//       print('Hidup tak selamanya tentang pacar.');
//       break;
//     case 'sabtu':
//       print('Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.');
//       break;
//     case 'minggu':
//       print(
//           'Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.');
//       break;
//     default:
//       print(
//           'Hari yang dimasukkan tidak valid. Mohon masukkan hari dalam format yang benar (contoh: Senin, Selasa, Rabu, dst).');
//       break;
//   }
// }

//Soal No.4
void main() {
  int hari = 21;
  int bulan = 1;
  int tahun = 1945;

  // Menggunakan switch case untuk format tanggal
  String namaBulan;
  switch (bulan) {
    case 1:
      namaBulan = 'Januari';
      break;
    case 2:
      namaBulan = 'Februari';
      break;
    case 3:
      namaBulan = 'Maret';
      break;
    case 4:
      namaBulan = 'April';
      break;
    case 5:
      namaBulan = 'Mei';
      break;
    case 6:
      namaBulan = 'Juni';
      break;
    case 7:
      namaBulan = 'Juli';
      break;
    case 8:
      namaBulan = 'Agustus';
      break;
    case 9:
      namaBulan = 'September';
      break;
    case 10:
      namaBulan = 'Oktober';
      break;
    case 11:
      namaBulan = 'November';
      break;
    case 12:
      namaBulan = 'Desember';
      break;
    default:
      namaBulan = 'Bulan tidak valid';
      break;
  }

  // Format tanggal sesuai dengan hasil switch case
  String tanggalFormat = '$hari $namaBulan $tahun';

  print('Tanggal: $tanggalFormat');
}
