// //soal No.1
// void main() {
//   // Memanggil function teriak() dan menampilkan nilai yang dikembalikan di console
//   print(teriak());
// }

// String teriak() {
//   // Function teriak() yang mengembalikan nilai "Halo Sanbers!"
//   return "Halo Sanbers!";
// }

// //soal No.2
// void main() {
//   var num1 = 12;
//   var num2 = 4;

//   // Memanggil function kalikan() dengan num1 dan num2 sebagai argumen
//   var hasilKali = kalikan(num1, num2);

//   // Menampilkan hasil perkalian
//   print(hasilKali); // Output: 48
// }

// // Function untuk melakukan perkalian dua angka
// int kalikan(int a, int b) {
//   return a * b;
// }

// //Soal No.3
// void main() {
//   // Memanggil function introduce() dengan memberikan nilai untuk parameter
//   String perkenalan =
//       introduce("Fahri", 27, "Jln. Dr saharjo tebet, jakarta", "Gaming");
//   print(perkenalan);
// }

// String introduce(String name, int age, String address, String hobby) {
//   // Function introduce() untuk memproses parameter menjadi kalimat perkenalan
//   return 'Nama saya $name, umur saya $age tahun, alamat saya di $address, dan saya punya hobby yaitu $hobby!';
// }

//Soal No.4
void main() {
  // Memanggil function faktorial dengan angka 6
  int angka = 6;
  int hasilFaktorial = faktorial(angka);
  print("$angka! = $hasilFaktorial");
}

int faktorial(int n) {
  // Validasi untuk angka <= 0, mengembalikan 1
  if (n <= 0) {
    return 1;
  }
  // Kasus rekursif untuk menghitung faktorial
  else {
    return n * faktorial(n - 1);
  }
}
