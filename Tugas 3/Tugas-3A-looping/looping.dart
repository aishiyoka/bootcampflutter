// //Soal No.1
// void main() {
//   // Looping Pertama
//   print('LOOPING PERTAMA');
//   int i = 2; // Mulai dari angka 2
//   while (i <= 20) {
//     // Loop sampai dengan angka 20
//     print('$i - I love coding');
//     i += 2; // Melangkah 2 demi 2 untuk bilangan genap
//   }

//   // Looping Kedua
//   print('LOOPING KEDUA');
//   int j = 20;
//   while (j >= 2) {
//     print('$j - I will become a mobile developer');
//     j -= 2; // Melangkah mundur 2 demi 2 dari 20 hingga 2
//   }
// }

// //Soal No.2
// void main() {
//   for (int i = 1; i <= 20; i++) {
//     if (i % 4 == 0) {
//       print('$i - Berkualitas');
//     } else if (i % 3 == 0) {
//       print('$i - I Love Coding');
//     } else if (i % 2 == 0) {
//       print('$i - Berkualitas');
//     } else {
//       print('$i - Santai');
//     }
//   }
// }

// //Soal No.3
// void main() {
//   // Loop untuk mencetak 4 baris
//   for (int i = 0; i < 4; i++) {
//     String line = '';
//     // Loop untuk mencetak 8 tanda pagar (#) pada setiap baris
//     for (int j = 0; j < 8; j++) {
//       // Cetak tanda pagar (#)
//       line += '#';
//     }
//     // Cetak baris kosong setelah mencetak satu baris penuh tanda pagar
//     print(line);
//   }
// }

//Soal No.4
void main() {
  // Loop untuk mengontrol jumlah baris
  for (int i = 1; i <= 5; i++) {
    // String kosong untuk menyimpan hasil cetakan pada baris saat ini
    String line = '';

    // Loop untuk mencetak tanda pagar (#) sebanyak i kali pada baris ke-i
    for (int j = 1; j <= i; j++) {
      line +=
          j.toString(); // Tambahkan satu tanda pagar (#) ke dalam string line
    }

    // Cetak hasil dari baris saat ini
    print(line);
  }
}
