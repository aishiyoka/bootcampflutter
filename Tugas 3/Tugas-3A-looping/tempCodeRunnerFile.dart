void main() {
  // Loop untuk mengontrol jumlah baris
  for (int i = 1; i <= 5; i++) {
    // String kosong untuk menyimpan hasil cetakan pada baris saat ini
    String line = '';

    // Loop untuk mencetak tanda pagar (#) sebanyak i kali pada baris ke-i
    for (int j = 1; j <= i; j++) {
      line += j.toString(); // Tambahkan satu tanda pagar (#) ke dalam string line
    }

    // Cetak hasil dari baris saat ini
    print(line);
  }
}