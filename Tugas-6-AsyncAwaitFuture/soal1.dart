import 'dart:async';

void main() async {
  print('luffy');
  print('zoro');
  print('killer');

  // Menunggu 3 detik menggunakan async dan await
  await Future.delayed(Duration(seconds: 3));

  print('get data [done]');
  print('name 3: hilmy');
}
