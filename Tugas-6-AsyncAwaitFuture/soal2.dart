import 'dart:async';

void main(List<String> args) {
  print("life");

  // Menunda eksekusi selama 0 detik untuk menjamin urutan eksekusi yang benar
  Future.delayed(Duration(seconds: 0), () {
    print("is");
  });

  Future.delayed(Duration(seconds: 0), () {
    print("Never flat");
  });
}
