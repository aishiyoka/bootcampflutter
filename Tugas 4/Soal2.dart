List<int> rangeWithStep(int startNum, int finishNum, int step) {
  List<int> result = [];

  if (startNum <= finishNum) {
    // Generate ascending sequence with step
    for (int i = startNum; i <= finishNum; i += step) {
      result.add(i);
    }
  } else {
    // Generate descending sequence with step
    for (int i = startNum; i >= finishNum; i -= step) {
      result.add(i);
    }
  }

  return result;
}

void main() {
  print(rangeWithStep(1, 10, 1)); // Output: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  print(rangeWithStep(11, 23, 3)); // Output: [11, 14, 17, 20, 23]
  print(rangeWithStep(5, 2, 1)); // Output: [5, 4, 3, 2]
}
