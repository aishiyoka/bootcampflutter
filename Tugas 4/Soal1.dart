List<int> range(int startNum, int finishNum) {
  List<int> result = [];

  if (startNum <= finishNum) {
    // Generate ascending sequence
    for (int i = startNum; i <= finishNum; i++) {
      result.add(i);
    }
  } else {
    // Generate descending sequence
    for (int i = startNum; i >= finishNum; i--) {
      result.add(i);
    }
  }

  return result;
}

void main() {
  print(range(1, 10)); // Output: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  print(range(11, 18)); // Output: [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
}
