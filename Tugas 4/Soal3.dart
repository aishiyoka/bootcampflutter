// void dataHandling(List<List<String>> inputData) {
//   for (List<String> data in inputData) {
//     String id = data[0];
//     String name = data[1];
//     String city = data[2];
//     String birthdate = data[3];
//     String hobby = data[4];

//     print("ID: $id");
//     print("Name: $name");
//     print("City: $city");
//     print("Birthdate: $birthdate");
//     print("Hobby: $hobby");
//     print(""); // Print empty line for separation
//   }
// }

// void main() {
//   var input = [
//     ["001", "Roman Alamsyah", "Banda Lampung", "21/05/1989", "Membaca"],
//     ["002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain gitar"],
//     ["003", "Winona", "Ambon", "25/12/1965", "Memasak"],
//     ["004", "Bintang Sanjaya", "Martapura", "6/4/1970", "Berkebun"]
//   ];

//   // Call the dataHandling function with input data
//   dataHandling(input);
// }

