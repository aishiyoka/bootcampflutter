import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';

void main() {
  ArmorTitan armorTitan = ArmorTitan();
  armorTitan.powerPoint = 7;
  print("Armor Titan Power Point: ${armorTitan.powerPoint}");
  print("Action: ${armorTitan.action()}");

  AttackTitan attackTitan = AttackTitan();
  attackTitan.powerPoint = 3;
  print("Attack Titan Power Point: ${attackTitan.powerPoint}");
  print("Action: ${attackTitan.action()}");

  BeastTitan beastTitan = BeastTitan();
  beastTitan.powerPoint = 9;
  print("Beast Titan Power Point: ${beastTitan.powerPoint}");
  print("Action: ${beastTitan.action()}");

  Human human = Human();
  human.powerPoint = 2;
  print("Human Power Point: ${human.powerPoint}");
  print("Action: ${human.action()}");
}
