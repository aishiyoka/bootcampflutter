class Titan {
  int _powerPoint;

  Titan({int powerPoint = 0}) : _powerPoint = powerPoint;

  int get powerPoint => _powerPoint;

  set powerPoint(int value) {
    _powerPoint = value;
  }

  String action() {
    return "Default action";
  }
}
