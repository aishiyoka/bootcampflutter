import 'employee.dart';

void main() {
  // Membuat objek employee
  Employee employee1 = Employee(id: 1, name: "John Doe", department: "HR");
  Employee employee2 =
      Employee(id: 2, name: "Jane Smith", department: "Finance");
  Employee employee3 = Employee(id: 3, name: "Alice Johnson", department: "IT");

  // Menampilkan informasi employee
  print("Employee 1:");
  print("ID: ${employee1.id}");
  print("Name: ${employee1.name}");
  print("Department: ${employee1.department}");

  print("\nEmployee 2:");
  print("ID: ${employee2.id}");
  print("Name: ${employee2.name}");
  print("Department: ${employee2.department}");

  print("\nEmployee 3:");
  print("ID: ${employee3.id}");
  print("Name: ${employee3.name}");
  print("Department: ${employee3.department}");
}
