import 'dart:math';

class Lingkaran {
  double _radius = 0; // Inisialisasi _radius dengan nilai default 0

  Lingkaran(double radius) {
    _radius = radius.abs(); // Mengambil nilai absolut agar selalu positif
  }

  // Getter untuk mendapatkan nilai radius
  double get radius => _radius;

  // Setter untuk mengubah nilai radius
  set radius(double value) {
    _radius = value.abs(); // Mengambil nilai absolut agar selalu positif
  }

  // Method untuk menghitung luas lingkaran
  double hitungLuas() {
    return pi * _radius * _radius;
  }
}

void main() {
  double radiusInput = -5; // Contoh input radius
  Lingkaran lingkaran = Lingkaran(radiusInput);

  // Cetak nilai radius dan luas lingkaran
  print("Radius lingkaran: ${lingkaran.radius}");
  print("Luas lingkaran: ${lingkaran.hitungLuas()}");
}
