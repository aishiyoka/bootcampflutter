import 'dart:async';
import 'package:JobNight/Latihan-1/model/postmodel.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class Services {
  Future<List<PostModel>?> getAllPosts() async {
    try {
      var response = await http
          .get(
        Uri.parse("https://jsonplaceholder.typicode.com/posts"),
      )
          .timeout(
        const Duration(seconds: 10),
        onTimeout: () {
          throw TimeoutException("Connection timed out. Please try again.");
        },
      );

      if (response.statusCode == 200) {
        List jsonResponse = convert.jsonDecode(response.body);
        return jsonResponse.map((e) => PostModel.fromJson(e)).toList();
      } else {
        print("Failed to load posts: ${response.statusCode}");
        return null;
      }
    } on TimeoutException catch (_) {
      print("Response timeout");
      return null;
    } catch (e) {
      print("Error fetching posts: $e");
      return null;
    }
  }
}
