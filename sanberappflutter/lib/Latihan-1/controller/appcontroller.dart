import 'package:JobNight/Latihan-1/model/postmodel.dart';
import 'package:JobNight/Latihan-1/services/services.dart';
import 'package:get/get.dart';

class AppController extends GetxController {
  var getPosts = <PostModel>[].obs;
  Services services = Services();
  var postLoading = true.obs;

  @override
  void onInit() {
    callPostMethod();
    super.onInit();
  }

  void callPostMethod() async {
    try {
      postLoading.value = true;
      var result = await services.getAllPosts();
      if (result != null) {
        getPosts.assignAll(result);
      } else {
        print("No posts found");
      }
    } catch (e) {
      print("Error fetching posts: $e");
    } finally {
      postLoading.value = false;
      update();
    }
  }
}
