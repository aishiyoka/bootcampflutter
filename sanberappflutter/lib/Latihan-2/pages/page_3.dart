import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:JobNight/Latihan-2/routes/route_name.dart';

class PageThree extends StatelessWidget {
  const PageThree({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Halaman 3"),
            ElevatedButton(
              onPressed: () {
                Get.toNamed(RouteName.page_1);
              },
              child: Text("Kembali ke Page 1"),
            ),
          ],
        ),
      ),
    );
  }
}
