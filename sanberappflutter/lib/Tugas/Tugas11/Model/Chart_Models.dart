class ChartModel {
  final String? name;
  final String? message;
  final String? time;
  final String? profileUrl;

  ChartModel({this.name, this.message, this.time, this.profileUrl});
}

// Initialize the items list separately
final List<ChartModel> items = [
  ChartModel(
    name: 'hilmy',
    message: 'Hello Hilmy',
    time: '12:00',
    profileUrl:
        'https://asset.kompas.com/crops/IjmmRwybxe-4tZrGRN5eMrq_po8=/0x32:70x382/75x500/data/photo/2020/01/09/5e16811892fc7.jpg',
  ),
  ChartModel(
    name: 'riska',
    message: 'Hello Riska',
    time: '9 March',
    profileUrl:
        'https://img.okezone.com/content/2019/04/30/298/2049787/chef-arnold-berbagi-tips-membuat-makanan-menjadi-lebih-enak-Tz2kVDuNL2.jpg',
  ),
  ChartModel(
    name: 'vita',
    message: 'Hello Vita',
    time: '10 March',
    profileUrl:
        'https://asset.kompas.com/crops/ployX7cQOqsYqJS2PYvUGv41CaI=/0x0:1000x667/75x500/data/photo/2017/06/22/163146320170622-42902-8311-chef-juna-atau-junior-rorimpandey.jpg',
  ),
];
