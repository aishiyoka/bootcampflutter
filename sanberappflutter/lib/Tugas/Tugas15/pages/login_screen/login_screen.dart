import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:JobNight/Tugas/Tugas15/config/dio_http.dart';
// import 'package:rest_api_example/pages/register_screen/register_screen.dart';
import 'package:JobNight/Tugas/Tugas15/pages/register_screen/register_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:JobNight/Tugas/Tugas15/pages/main_app/main_screen.dart';
// import '../main_app/main_app.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _passwordVisible = false;
  bool _isLoading = false;
  late String email;
  late String password;

  @override
  void initState() {
    super.initState();
    _passwordVisible = false;
  }

  Future<void> login() async {
    var data = {"email": email, "password": password};
    try {
      setState(() {
        _isLoading = true;
      });
      var response = await DioHttp.request.post("/api/login", data: data);
      setState(() {
        _isLoading = false;
      });
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('user-token', response.data["token"]);
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => MainApp()),
      );
    } on DioError catch (dioError) {
      String message;
      setState(() {
        _isLoading = false;
      });
      if (dioError.response != null) {
        switch (dioError.response!.statusCode) {
          case 400:
            message = dioError.response!.data["message"].toString();
            break;
          case 404:
            message = "Server Not Found";
            break;
          default:
            message = "Server Error";
        }
      } else {
        message = "Unknown Error";
      }
      final snackBar = SnackBar(
        content: Text(message),
        action: SnackBarAction(
          label: 'Undo',
          onPressed: () {
            // Some code to undo the change.
          },
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: LoadingOverlay(
          isLoading: _isLoading,
          child: Container(
            padding: EdgeInsets.all(16),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Image.asset("assets/logo/logo.png"),
                  SizedBox(height: 15),
                  Text(
                    "JobFinder",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      color: Color(0xff475BD7),
                    ),
                  ),
                  SizedBox(height: 15),
                  Text(
                    "Explore Your Dream Jobs",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff475BD7),
                    ),
                  ),
                  SizedBox(height: 15),
                  Container(
                    height: 60,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(color: Colors.grey),
                    ),
                    child: Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.only(left: 10),
                      child: TextFormField(
                        decoration: InputDecoration(
                          hintText: "Enter your email",
                          hintStyle: TextStyle(color: Colors.black),
                          labelText: "Email",
                        ),
                        validator: (String? emailValue) {
                          if (emailValue!.isEmpty) {
                            return "Please enter email";
                          }
                          email = emailValue;
                          return null;
                        },
                      ),
                    ),
                  ),
                  SizedBox(height: 15),
                  Container(
                    height: 60,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(color: Colors.grey),
                    ),
                    child: Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.only(left: 10),
                      child: TextFormField(
                        obscureText: !_passwordVisible,
                        decoration: InputDecoration(
                          hintText: "Enter your password",
                          hintStyle: TextStyle(color: Colors.black),
                          labelText: "Password",
                          suffixIcon: IconButton(
                            icon: Icon(
                              _passwordVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Theme.of(context).primaryColorDark,
                            ),
                            onPressed: () {
                              setState(() {
                                _passwordVisible = !_passwordVisible;
                              });
                            },
                          ),
                        ),
                        validator: (String? passwordValue) {
                          if (passwordValue!.isEmpty) {
                            return "Please enter password";
                          }
                          password = passwordValue;
                          return null;
                        },
                      ),
                    ),
                  ),
                  SizedBox(height: 15),
                  Row(
                    children: [
                      Text("Don't have an account?"),
                      TextButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => RegisterScreen(),
                            ),
                          );
                        },
                        child: Text("Register"),
                      ),
                    ],
                  ),
                  Container(
                    height: 60,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: TextButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          login();
                        }
                      },
                      child: Text(
                        "Login",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
