import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          padding: EdgeInsets.all(16),
          margin: EdgeInsets.only(top: 40),
          color: Colors.white,
          child: ListView(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Let's Find",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.w800),
                  ),
                  Icon(
                    Icons.notifications,
                    color: Colors.grey,
                  ),
                ],
              ),
              SizedBox(height: 10),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Your Dream Jobs",
                  style: TextStyle(fontSize: 30),
                ),
              ),
              SizedBox(height: 20),
              Container(
                height: 50,
                width: double.infinity,
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: Colors.grey),
                ),
                child: TextFormField(
                  decoration: InputDecoration.collapsed(
                    hintText: "Search jobs or positions",
                  ),
                ),
              ),
              SizedBox(height: 20),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Jobs For You",
                  style: TextStyle(fontSize: 20),
                ),
              ),
              SizedBox(height: 20),
              jobsItem(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget jobsItem(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 2,
      child: GridView.count(
        primary: false,
        padding: const EdgeInsets.all(20),
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        crossAxisCount: 2,
        children: <Widget>[
          jobContainer("assets/img/gojek.png", "Digital Marketing",
              "1-3 Year Experience", "FullTime", "Senior"),
          jobContainer("assets/img/shopee.png", "Content Creator",
              "1-3 Year Experience", "FullTime", "Internship"),
          jobContainer("assets/img/bukalapak.png", "Front End Dev",
              "1-3 Year Experience", "FullTime", "Senior"),
          jobContainer("assets/img/blibli.png", "UX Designer",
              "1-3 Year Experience", "FullTime", "Senior"),
        ],
      ),
    );
  }

  Widget jobContainer(String image, String title, String experience,
      String type1, String type2) {
    return Container(
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: Colors.grey[200],
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            image,
            height: 75,
            width: 75,
          ),
          SizedBox(height: 16),
          Text(
            title,
            style: titleStyle(),
          ),
          SizedBox(height: 16),
          Text(
            experience,
            style: subTitle(),
          ),
          SizedBox(height: 16),
          Row(
            children: [
              Text(
                type1,
                style: positionText(),
              ),
              SizedBox(width: 16),
              Text(
                type2,
                style: positionText(),
              ),
            ],
          ),
        ],
      ),
    );
  }

  TextStyle positionText() {
    return TextStyle(color: Colors.black, fontWeight: FontWeight.w400);
  }

  TextStyle subTitle() {
    return TextStyle(fontSize: 12, fontWeight: FontWeight.w400);
  }

  TextStyle titleStyle() {
    return TextStyle(fontSize: 15, fontWeight: FontWeight.w500);
  }
}
