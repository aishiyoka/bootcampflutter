import 'package:flutter/material.dart';

class GetDataDetailScreen extends StatelessWidget {
  final int value;

  GetDataDetailScreen({required this.value});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail Screen"),
      ),
      body: Center(
        child: Text("Detail for item ID: $value"),
      ),
    );
  }
}
