import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
// import 'package:flutter_web/Tugas/Tugas12/LoginScreen.dart';
// import 'Tugas/Tugas11/Telegram.dart';
// import 'Tugas/Tugas13/DrawerScreen.dart';
// import 'Tugas/Tugas13/Search.dart';
// import 'Tugas/Tugas12/LoginScreen.dart';
// import 'Tugas/Tugas13/HomeScreen.dart';
// import 'Tugas/Tugas13/Account.dart';
// import 'package:JobNight/Latihan/authentication/homescreen.dart';
// import 'package:JobNight/Latihan/authentication/login_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.blue),
      // home: Telegram(),
      // home: MainScreen(),
      // home: HomeScreen(),
      // home: LoginScreen(),
    );
  }
}

// class MainScreen extends StatefulWidget {
//   @override
//   _MainScreenState createState() => _MainScreenState();
// }

// class _MainScreenState extends State<MainScreen> {
//   int _currentIndex = 0;

//   final List<Widget> _children = [
//     HomeScreen(),
//     SearchScreen(),
//     AccountScreen(),
//   ];

//   GlobalKey<ScaffoldState> _scaffoldKey =
//       GlobalKey<ScaffoldState>(); // Tambahkan kunci scaffold

//   void onTabTapped(int index) {
//     setState(() {
//       _currentIndex = index;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       key: _scaffoldKey,
//       drawer: DrawerScreen(),
//       body: _children[_currentIndex],
//       appBar: AppBar(
//         title: Text('JobNight'),
//         // Tambahkan ikon drawer di sini
//         leading: IconButton(
//           icon: Icon(Icons.menu),
//           onPressed: () {
//             _scaffoldKey.currentState!
//                 .openDrawer(); // Buka drawer ketika ikon ditekan
//           },
//         ),
//       ),
//       bottomNavigationBar: BottomNavigationBar(
//         onTap: onTabTapped,
//         currentIndex: _currentIndex,
//         items: [
//           BottomNavigationBarItem(
//             icon: Icon(Icons.home),
//             label: 'Home',
//           ),
//           BottomNavigationBarItem(
//             icon: Icon(Icons.search),
//             label: 'Search',
//           ),
//           BottomNavigationBarItem(
//             icon: Icon(Icons.account_circle),
//             label: 'Account',
//           ),
//         ],
//       ),
//     );
//   }
// }
